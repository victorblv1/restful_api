const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

// Middleware
app.use(cors());
app.use(bodyParser.json());

// Import routes.
const postRoute = require('./routes/posts');
app.use('/posts', postRoute);

// Create routes.
app.get('/', (req, res) => {
  res.send('Endpoint of the server.');
});

// Connect to db.
mongoose.connect('mongodb://localhost:27017/restapiservice',
    {useNewUrlParser: true, useUnifiedTopology: true}, () => {
      console.log('Rest API db is connected.');
    });

// Listener.
app.listen(3000, console.log('Server is running ...'));
